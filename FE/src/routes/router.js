import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../components/pages/Dashboard.vue";
import Patron from "../components/pages/Patron.vue";
import Book from "../components/pages/Books.vue";
import Settings from "../components/pages/Settings.vue";


Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [

    {
      path: "/",
      component: Dashboard,
    },
    {
      path: "/Dashboard",
      name: "Dashboard",
      component: Dashboard,
    },
    {
      path: "/Patron",
      name: "Patron",
      component: Patron,
    },

   {
      path: "/Book",
      name: "Book",
      component: Book,
    },
    
    {
      path: "/Settings",
      name: "Settings",
      component: Settings,
    },
    
  ],
});
