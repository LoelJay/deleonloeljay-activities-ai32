<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BorrowRequest extends FormRequest
{
   
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'book_id' => 'required|integer',
            'copies' => 'required|integer',
            'patron_id' => 'required|integer'
        ];
    }
}
