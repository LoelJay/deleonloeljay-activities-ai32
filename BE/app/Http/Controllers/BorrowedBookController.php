<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use App\Models\Book;
use App\Models\ReturnedBook;
use App\Http\Requests\BorrowRequest;
use Illuminate\Http\Request;

class BorrowedBookController extends Controller
{
   
    public function index()
    {
        $borrowedbook = BorrowedBook::all();
        return response()->json([
            "message" => "Borrowed Books",
            "data" => $borrowedbook]);
    }

   
    public function store(BorrowRequest $request)
    {
        $borbook = new BorrowedBook();
        $borbook->copies = $request->copies;
        $borbook->book_id = $request->book_id;
        $borbook->patron_id = $request->patron_id;

        $books = Book::find($borbook->book_id);

        $minuscopies = $books->copies - $borbook->copies;
        
        $validated = $request->validated();
        $borbook->save();
        $books->update(['copies' => $minuscopies]);
        return response()->json(
               ["message" => "BorrowedBook Success",
               "data" => $borbook, $books]);
    }

    
    public function show($id)
    {
        $borbook = BorrowedBook::find($id);
        return response()->json($borbook);
    }

    
    public function update(BorrowRequest $request, $id)
    {
        $borbook = BorrowedBook::find($id);
        $retbook = new ReturnedBook();
        
        $retbook->copies = $request->copies;
        $retbook->book_id = $request->book_id;
        $retbook->patron_id = $request->patron_id;

        $book = Book::find($retbook->book_id);

        $addcopiestobook = $book->copies + $retbook->copies;
        
        $validated = $request->validated();
        $retbook->save();
        $borbook->delete();
        $book->update(['copies' => $addcopiestobook]);
        return response()->json(
               ["message" => "BorrowedBook Success",
               "data" => $borbook,$book, $retbook]);
    }

   
}
