<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $books = Book::all();
            return response()->json([
            "message" => "Books",
            "data" => $books]);
    }

    
    public function store(BookRequest $request)
    {
        $books = new Book();

        $books->name =   $request->name;
        $books->author = $request->author;
        $books->copies = $request->copies;
        $books->category_id = $request->category_id;

        $validated = $request->validated();
        $books->save();
        return response()->json($books);
    }


    public function show($id)
    {
        $books = Book::find($id);
        return response()->json($books);
    }
   

    public function update(BookRequest $request, $id)
    {
        $books = Book::find($id);
        $books->name = $request->name;
        $books->author = $request->author;
        $books->copies = $request->copies;
        $books->category_id = $request->category_id;
       
        $validated = $request->validated();
        $books->update();
        return response()->json($books);
    }

}
