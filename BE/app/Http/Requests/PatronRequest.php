<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatronRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'last_name' => 'required|max:50',
            'first_name' => 'required|max:50',
            'middle_name' => 'required|max:20',
            'email' => 'required|email:rfc,dns'
        ];
    }
}
