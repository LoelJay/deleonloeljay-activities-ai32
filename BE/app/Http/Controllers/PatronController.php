<?php

namespace App\Http\Controllers;

use App\Models\Patron;
use App\Http\Requests\PatronRequest;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    
    public function index()
    {
       
        $patrons = Patron::all();
        return response()->json([
        "message" => "Patrons",
        "data" => $patrons]); 
    }

    
    public function store(PatronRequest $request)
    {
        $patrons = New Patron();

        $patrons->last_name = $request->last_name;
        $patrons->first_name = $request->first_name;
        $patrons->middle_name = $request->middle_name;
        $patrons->email = $request->email;
       
        $validated = $request->validated();
        $patrons->save();
        return response()->json($patrons);

    }
    

    public function show($id)
    {
        $patrons = Patron::find($id);
        return response()->json($patrons);
    }

    
    public function update(PatronRequest $request, $id)
    {
        $patrons = Patron::find($id);
        $patrons->last_name = $request->last_name;
        $patrons->first_name = $request->first_name;
        $patrons->middle_name = $request->middle_name;
        $patrons->email = $request->email;

        $validated = $request->validated();
        $patrons->update();
        return response()->json($patrons);
    }

    
   
}
