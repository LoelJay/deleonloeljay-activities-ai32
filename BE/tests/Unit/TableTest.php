<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\Category;
use App\Models\Patron;
use App\Models\ReturnedBook;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;


class TableTest extends TestCase
{
    public function test_a_patrons_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('patrons', [
            'id', 'last_name', 'first_name', 'middle_name', 'email'
        ]), 1);
    }
    

    public function test_a_books_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('books', [
            'id', 'name', 'author', 'copies', 'category_id'
        ]), 1);
    }

    public function test_a_categories_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('categories', [
            'id', 'category'
        ]), 1);
    }

    public function test_a_borrowed_books_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('borrowed_books', [
            'id', 'patron_id', 'copies', 'book_id'
        ]), 1);
    }

    public function test_a_returned_books_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('returned_books', [
            'id', 'patron_id', 'copies', 'book_id'
        ]), 1);
    }
  
}
