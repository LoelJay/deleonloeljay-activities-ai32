<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    
    public function run()
    {

        $multi_rows = [

            ['category' => 'Horror'],
            ['category' => 'Fantasy'],
            ['category' => 'Romance'],
            ['category' => 'History'],
            ['category' => 'Science Fiction']
        ];

        foreach ($multi_rows as $rows) {
            category::create($rows);
        }
        
    }
}
